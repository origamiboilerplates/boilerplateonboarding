package com.adc.origamistudios.utilities;

public interface Constants {
    int minTime = 1000;
    int maxTime = 9000;
    int SPLASH_DELAY_TIME = 3000;

    /////////////////////////////////Toast & Alert Messages
    String kStringTitlePermission = "Permission necessary";
    String kStringExternalStoragePermission = "External storage permission is necessary";


    //////////////////////////Shared Preferences
    String PREF_IS_SKIP = "onBoardSkip";
}
