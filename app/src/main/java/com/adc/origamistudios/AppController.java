package com.adc.origamistudios;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class AppController extends MultiDexApplication {

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }

}
