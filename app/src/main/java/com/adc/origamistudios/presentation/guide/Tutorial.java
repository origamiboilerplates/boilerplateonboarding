package com.adc.origamistudios.presentation.guide;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.adc.origamistudios.R;
import com.adc.origamistudios.presentation.pager.TabAdapters;
import com.adc.origamistudios.utilities.Constants;
import com.adc.origamistudios.widgets.CustomTextView;

import me.relex.circleindicator.CircleIndicator;

public class Tutorial extends FragmentActivity implements Constants, View.OnClickListener {


    private static final int CAMERA_MIC_PERMISSION_REQUEST_CODE = 11;
    private CustomTextView skipTv;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_tutorial);
        initData();
        setOnClickListeners();
    }

    private void initData() {
        ViewPager viewPager = findViewById(R.id.pager);
        CircleIndicator indicator = findViewById(R.id.indicator);
        skipTv = findViewById(R.id.skipTv);
        //skipTv.setTextColor(getResources().getColor(R.color.white));
        //TextView tvTitle = findViewById(R.id.header_title);
        //tvTitle.setTextColor(getResources().getColor(R.color.white));
        //ivBack = (ImageView) findViewById(R.id.ivToolbarBack);
        //ivBack.setImageResource(R.drawable.back_arrow);
        //ivBack.setVisibility(View.GONE);
        TabAdapters page_adapter = new TabAdapters(getSupportFragmentManager());
        viewPager.setAdapter(page_adapter);
        indicator.setViewPager(viewPager);

    }

    private void setOnClickListeners() {
        skipTv.setOnClickListener(this);
        // ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == skipTv) {
           /* Utilities.getInstance(this).saveBooleanPreferences(PREF_IS_SKIP, true);
            startActivity(new Intent(this, SignUpActivity.class));
            finish();*/
        } else if (v == ivBack) {
            //startActivity(new Intent(getApplicationContext(), SignInActivity.class));
            //finish();
        }

    }

    private void requestPermissionForCameraAndMicrophone() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                CAMERA_MIC_PERMISSION_REQUEST_CODE);
    }
}
