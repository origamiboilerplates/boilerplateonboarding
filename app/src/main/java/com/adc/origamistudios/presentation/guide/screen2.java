package com.adc.origamistudios.presentation.guide;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adc.origamistudios.R;


public class screen2 extends Fragment {
    ImageView content;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.tutorialscreen2
                , container, false);
    }


}
