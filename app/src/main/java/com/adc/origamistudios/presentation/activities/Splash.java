package com.adc.origamistudios.presentation.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.adc.origamistudios.R;
import com.adc.origamistudios.presentation.guide.Tutorial;
import com.adc.origamistudios.utilities.Constants;
import com.adc.origamistudios.utilities.Utilities;

import static com.adc.origamistudios.utilities.Constants.SPLASH_DELAY_TIME;


public class Splash extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent;
                if (Utilities.getInstance(getApplicationContext()).getBooleanPreferences(Constants.PREF_IS_SKIP)) {
                    mainIntent = new Intent(Splash.this, Tutorial.class);
                } else {
                    mainIntent = new Intent(Splash.this, Tutorial.class);
                }

                startActivity(mainIntent);
                finish();


            }
        }, SPLASH_DELAY_TIME);
    }
}
