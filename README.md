# Android BoilerplateOnBoarding

Android BoiletplateOnBoarding is build to start any new app with splash and onboarding screen integration.


## Step 1

To start new android app just clone the android BoilerplateOnBoarding project from following link. https://bitbucket.org/origamiboilerplates/boilerplateonboarding


### How to add in new project

To add splash and on boarding screen in any new project, just copy presentation package and add it in your app project. Update splash and onboarding screen UI according to your app desing. Add splash and onboarading activities in you app manifest file.

```
 <activity
            android:name="com.adc.origamistudios.presentation.activities.Splash"
            android:screenOrientation="portrait"
            android:theme="@android:style/Theme.Black.NoTitleBar.Fullscreen">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>

        <activity
            android:name="com.adc.origamistudios.presentation.guide.Tutorial"
            android:screenOrientation="portrait"
            android:theme="@android:style/Theme.Black.NoTitleBar.Fullscreen" />
            

```
